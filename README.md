# Getting started

You can simply use docker to run this application. After checking out the repository via git, you can run `docker-compose up` to spin up the application. You can then access the API via http://localhost:8000/api/user/{username}

Example data is provided under the username `rasika`, you can access this data via http://localhost:8000/api/user/rasika

The create link API is yet to be implemented, but you can add new data to test via the Django admin at http://localhost:8000/admin/

Default admin user credentials are rasika/admin.
