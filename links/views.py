from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404

from rest_framework.views import APIView
from rest_framework.response import Response

from links import serialisers

class UserLinksView(APIView):
    """User links API view."""

    def get(self, _, username, **kwargs):
        """Handle the GET method."""
        user = get_object_or_404(User, username=username)
        serialised = serialisers.UserLinksSerialiser(user)
        return Response(serialised.data)


class CreateUserLinkView(APIView):
    """API view to create a new link."""

    def post(self, _, username, **kwargs):
        """Handle the POST method to create a new link."""
        # TODO: Implement the create link logic using the NewLinkSerialiser
