from django.contrib import admin
from links import models

admin.site.register(models.ClassicLink)
admin.site.register(models.MusicPlayerService)
admin.site.register(models.MusicPlayerLink)
admin.site.register(models.ShowsListLink)
admin.site.register(models.Show)
