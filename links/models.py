from django.conf import settings
from django.db import models


class BaseLink(models.Model):
    """Base link."""

    class Meta:
        """Meta class for a base link."""
        abstract = True

    title = models.CharField(max_length=144)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )
    date_created = models.DateTimeField(auto_now_add=True)
    date_last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        """String representation of an object."""
        return self.title


class ClassicLink(BaseLink):
    """A classic link."""

    url = models.URLField()


class MusicPlayerService(models.Model):
    """A music service."""

    name = models.CharField(max_length=144)
    service_url = models.URLField()
    embedded_player_template = models.CharField(max_length=500)

    def __str__(self):
        """String representation of an object."""
        return self.name


class MusicPlayerLink(BaseLink):
    """A music player link."""

    service = models.ForeignKey(
        MusicPlayerService,
        on_delete=models.CASCADE
    )
    url = models.URLField()

    def get_embedded_player(self):
        """Get the embeded music player."""
        return self.service.embedded_player_template.format(self.url)


class ShowsListLink(BaseLink):
    """A shows list link."""

    pass


class Show(models.Model):
    """A show."""

    PENDING_SALE = 'PENDING'
    SOLD_OUT = 'SOLDOUT'
    ON_SALE = 'ONSALE'

    SHOW_STATUS_CHOICES = [
        (PENDING_SALE, 'Pending Sale'),
        (SOLD_OUT, 'Sold Out'),
        (ON_SALE, 'On Sale'),
    ]

    date = models.DateField()
    venue_name = models.CharField(max_length=144)
    suburb = models.CharField(max_length=144)
    status = models.CharField(
        max_length=10,
        choices=SHOW_STATUS_CHOICES,
        default=ON_SALE
    )
    url = models.URLField()
    shows_list = models.ForeignKey(
        ShowsListLink,
        on_delete=models.CASCADE
    )

    def __str__(self):
        """String representation of an object."""
        return f'{self.date} {self.venue_name} {self.suburb}'
