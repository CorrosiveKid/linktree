from multiprocessing import managers
from django.contrib.auth.models import User
from rest_framework import serializers

from links import models


class ClassicLinkSerialiser(serializers.ModelSerializer):
    """Classic link serialiser."""

    class Meta:
        """Classic link serialiser meta options."""
        model = models.ClassicLink
        fields = '__all__'


class MusicPlayerLinkSerialiser(serializers.ModelSerializer):
    """Music player link serialiser."""

    embedded_player_html = serializers.SerializerMethodField()

    def get_embedded_player_html(self, object):
        """Get embedded player html."""
        return object.get_embedded_player()

    class Meta:
        """Music player link serialiser meta options."""
        model = models.MusicPlayerLink
        fields = '__all__'


class ShowSerialiser(serializers.ModelSerializer):
    """Show serialiser."""

    class Meta:
        """Show serialiser meta options."""
        model = models.Show
        fields = '__all__'


class ShowsListLinkSerialiser(serializers.ModelSerializer):
    """Shows list link serialiser."""

    shows = ShowSerialiser(many=True, source='show_set')

    class Meta:
        """Shows list link serialiser meta options."""
        model = models.ShowsListLink
        fields = '__all__'


class UserLinksSerialiser(serializers.Serializer):
    """User links serialiser."""
    classic_links = ClassicLinkSerialiser(many=True, source='classiclink_set')
    music_links = MusicPlayerLinkSerialiser(many=True, source='musicplayerlink_set')
    show_links = ShowsListLinkSerialiser(many=True, source='showslistlink_set')


class NewLinkSerialiser(serializers.Serializer):
    """A serialiser to create a new link."""
    # TODO: Implement a write-only serialiser to create a new link
    # TODO: Make sure only authenticated users can create links under their own profile
