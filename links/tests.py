from django.contrib.auth.models import User
from django.test import TestCase

from links import models

class TestMusicPlayerLink(TestCase):
    """Test music player link."""

    def test_get_embedded_player(self):
        user = User.objects.create_user('user1')

        service = models.MusicPlayerService(
            name="Spotify",
            service_url="https://open.spotify.com/",
            embedded_player_template="<iframe src='{}' width='300' height='380' frameborder='0' allowtransparency='true' allow='encrypted-media'></iframe>"
        )
        service.save()
        service.refresh_from_db()


        spotify_url = "https://open.spotify.com/embed/album/1DFixLWuPkv3KT3TnV35m3"
        music_link = models.MusicPlayerLink(
            user=user,
            service=service,
            url=spotify_url
        )
        music_link.save()
        music_link.refresh_from_db()
        embeded_player = music_link.get_embedded_player()
        self.assertEqual(
            embeded_player,
            "<iframe src='https://open.spotify.com/embed/album/1DFixLWuPkv3KT3TnV35m3' width='300' height='380' frameborder='0' allowtransparency='true' allow='encrypted-media'></iframe>"
        )


# TODO: Add API integration tests
