from django.urls import path
from links import views


urlpatterns = [
    path('user/<username>', views.UserLinksView.as_view()),
    path('user/<username>/link', views.CreateUserLinkView.as_view()),
]
